﻿using System;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace LogParser
{
    class Program
    {
        static void Main(string[] args)
        {
            bool toTerminate = false;
            LogParserInput logParserInput = new LogParserInput(args);
            CommandLineParams inputParams = logParserInput.ParseCommandLine();
            toTerminate = !logParserInput.ValidateInput(inputParams);

            Console.ForegroundColor = ConsoleColor.Red;
            if (inputParams.Err != null)
            {
                Console.WriteLine(inputParams.Err);
                toTerminate = true;
            }
            if (inputParams.NeedHelp)
            {
                inputParams.PrintHelp();
                toTerminate = true;
            }
            Console.ResetColor();
            if (toTerminate)
            {
                return;
            }
            LogToCSVConverter converter = new LogToCSVConverter(inputParams);
            converter.ConvertLogs();
        }

        private static void sampleMethod()
        {
            var patternString = @"(?m)^(?<ProcessID>\d{4}:\d{4})\s+(?<DTime>\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2}\.\d+)\s+(?<Type>\w+)\s+(?<Message>(?s:.*?(?=\n\d+:\d+|\r?\z)))";
            var stringToMatch = @"5376:0084 2015-08-07 13:51:29.103 Error ### Error Message ###
            5376:0084 2015-08-07 13:51:35.545 Error Discarding invalid session
            System.Web.Services.Protocols.SoapException: Verify Session ID failed
            at System.Web.Services.Protocols.SoapHttpClientProtocol.ReadResponse(SoapClientMessage message, WebResponse response, Stream responseStream, Boolean asyncCall)
            at System.Web.Services.Protocols.SoapHttpClientProtocol.Invoke(String methodName, Object[] parameters)
            5376:0084 2015-08-07 13:51:36.013 Error ### Error Message ###";

            Regex pattern = new Regex(patternString);
            Match match = pattern.Match(stringToMatch);

            var imageWidth = (match.Groups["ProcessID"].Value);
            var imageHeight = (match.Groups["DTime"].Value);
            var thumbWidth = (match.Groups["Type"].Value);
            var thumbHeight = (match.Groups["Message"].Value);
            Console.WriteLine("Width: " + imageWidth + ", Height: " + imageHeight);
            Console.WriteLine("Width: " + thumbWidth + ", Height: " + thumbHeight);

        }
    }
}
