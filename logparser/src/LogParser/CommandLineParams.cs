using System;
using System.Collections.Generic;
using System.Collections;


namespace LogParser
{
    public class CommandLineParams
    {
        public const string COMMAND_LOG_DIR = "--log-dir";
        public const string COMMAND_LOG_LEVEL = "--log-level";
        public const string COMMAND_LOG_OUTPUT = "--csv";
        public const string COMMAND_LOG_HELP = "--how";

        public static readonly string[] LOG_LEVELS = {"TRACE","DEBUG","INFO","WARN","ERROR"};


        public static readonly Dictionary<string, string> DICT = new Dictionary<string, string>() {
                {COMMAND_LOG_DIR,"Path for reading log dir (Required param)"},
                {COMMAND_LOG_LEVEL,"Logging level to parse in log file, pass multiple times with values. If not mentioned all logs will be processed"},
                {COMMAND_LOG_OUTPUT,"Output csv file path. If not provide output will be saved in current dir with name ddmmyy_hhmmss.csv."},
                {COMMAND_LOG_HELP,"Prints this help"}
            };
        public CommandLineParams()
        {
            Levels = new List<string>();
        }
        public bool NeedHelp { get; set; }
        public List<string> Levels { get; set; }
        public string InputPath { get; set; }
        public string OutputFilePath { get; set; }
        public string Err { get; set; }

        public override string ToString()
        {
            return $"Params: {Err}|{NeedHelp}|{InputPath}|{OutputFilePath}|{Levels.Count}";
        }
        public bool IsValidInput()
        {
            if (!NeedHelp && Err==null && InputPath == null)
            {
                Err = "Missing required params";
                NeedHelp = true;
                return false;
            }
            return true;
        }
        public void PrintHelp()
        {
            Console.WriteLine("Usage: LogParser.exe --log-dir <dir> --log-level <level> --csv <out>");
            foreach (var cmd in DICT)
            {
                Console.WriteLine("\t{0,-20} {1,1}", cmd.Key, cmd.Value);
            }
        }
    }
}