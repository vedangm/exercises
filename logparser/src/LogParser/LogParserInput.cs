using System;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
namespace LogParser
{
    public class LogParserInput
    {
        public string[] CmdArguments{get;set;}

        public LogParserInput(string[] arguments) {
            CmdArguments = arguments;
        }
        public CommandLineParams ParseCommandLine()
        {
            string[] arguments = CmdArguments;
            List<string> supportedArgs = CommandLineParams.DICT.Keys.ToList();
            CommandLineParams cmdParams = new CommandLineParams();
            if (arguments == null || arguments.Length == 0)
            {
                cmdParams.NeedHelp = true;
                return cmdParams;
            }
            for (int i = 0; i < arguments.Length; i++)
            {
                var command = arguments[i].ToLower();
                if (command.StartsWith("--") && supportedArgs.Contains(command))
                {
                    switch (command)
                    {
                        case CommandLineParams.COMMAND_LOG_DIR:
                            if (i < arguments.Length - 1 && !arguments[i + 1].StartsWith("--"))
                            {
                                cmdParams.InputPath = arguments[++i];
                            }
                            else
                            {
                                cmdParams.Err = $"Invalid input for {CommandLineParams.COMMAND_LOG_DIR}";
                                cmdParams.NeedHelp = true;
                            }
                            break;
                        case CommandLineParams.COMMAND_LOG_LEVEL:
                            if (i < arguments.Length - 1 && !arguments[i + 1].StartsWith("--"))
                            {
                                cmdParams.Levels.Add(arguments[++i]);
                            }
                            else
                            {
                                cmdParams.Err = $"Invalid input for {CommandLineParams.COMMAND_LOG_LEVEL}";
                                cmdParams.NeedHelp = true;
                            }
                            break;
                        case CommandLineParams.COMMAND_LOG_OUTPUT:
                            if (i < arguments.Length - 1 && !arguments[i + 1].StartsWith("--"))
                            {
                                cmdParams.OutputFilePath = arguments[++i];
                            }
                            else
                            {
                                cmdParams.Err = $"Invalid input for {CommandLineParams.COMMAND_LOG_OUTPUT}";
                                cmdParams.NeedHelp = true;
                            }
                            break;
                        case CommandLineParams.COMMAND_LOG_HELP:
                        default:
                            cmdParams.NeedHelp = true;
                            break;
                    }
                }
                else
                {
                    cmdParams.NeedHelp = true;
                    cmdParams.Err = $"Unknown command {command}";
                    break;
                }
            }
            return cmdParams;
        }

        public bool ValidateInput(CommandLineParams cmdParams)
        {
            if (!cmdParams.NeedHelp && cmdParams.IsValidInput())
            {
                return IsValidLogLevel(cmdParams) && IsValidInputPath(cmdParams) && IsValidOutputPath(cmdParams);
            }
            else
            {
                return false;
            }
        }
        private static bool IsValidInputPath(CommandLineParams cmdParams)
        {
            var inputDir = cmdParams.InputPath;
            if (!Directory.Exists(inputDir))
            {
                cmdParams.Err = $"Invalid Input Directory {inputDir}";
                cmdParams.NeedHelp = false;
                return false;
            }
            else if (Directory.GetFiles(inputDir, "*", SearchOption.AllDirectories).Length == 0)
            {
                cmdParams.Err = $"No Log files present in {inputDir}";
                cmdParams.NeedHelp = false;
                return false;
            }
            return true;
        }

        private static bool IsValidLogLevel(CommandLineParams cmdParams)
        {
            foreach (var loglevel in cmdParams.Levels)
            {
                if (!CommandLineParams.LOG_LEVELS.Contains(loglevel.ToUpper()))
                {
                    cmdParams.Err = $"Incorrect Log level. Supported log levels are {string.Join(", ", CommandLineParams.LOG_LEVELS)}";
                    cmdParams.NeedHelp = false;
                    return false;
                }
            }
            return true;
        }

        private static bool IsValidOutputPath(CommandLineParams cmdParams)
        {
            var outputDir = cmdParams.OutputFilePath;
            if(outputDir == null) {
                cmdParams.OutputFilePath = DateTime.Now.ToString("ddMMyyyy_HHmmss")+".csv";
            }
            else if(File.Exists(outputDir)) {
                cmdParams.Err = $"Output file {outputDir} already exists, Change output file name";
                cmdParams.NeedHelp = false;
                return false;
            }
            else if(Directory.Exists(outputDir)) {
                
                cmdParams.OutputFilePath+= Path.DirectorySeparatorChar+ DateTime.Now.ToString("ddMMyyyy_HHmmss")+".csv";
            }
            else if (!Directory.Exists(Path.GetDirectoryName(outputDir)))
            {
                cmdParams.Err = $"Output path {outputDir} does not exists";
                cmdParams.NeedHelp = false;
                return false;
            }
            return true;
        }
    }
}