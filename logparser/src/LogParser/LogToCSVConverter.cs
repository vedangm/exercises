using System;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
namespace LogParser
{
    public class LogToCSVConverter
    {
        public CommandLineParams CmdParams { get; set; }
        private static int serialNumber = 1;
        public LogToCSVConverter(CommandLineParams input)
        {
            CmdParams = input;
        }
        public void ConvertLogs()
        {
            var patternString = ApplicableRegEx();
            Regex pattern = new Regex(patternString);

            string[] files = Directory.GetFiles(CmdParams.InputPath, "*", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                try
                {
                    Console.WriteLine($"Processing File {file}");
                    //TODO: Add Try catch
                    List<LogEntry> logEntries = ProcessFile(file, pattern);
                    WriteLogToCSV(logEntries);
                }
                catch (Exception e) when (e is IOException || e is ArgumentNullException)
                {
                    Console.Error.WriteLine($"Error occurred while processing File {file}");
                    Console.Error.WriteLine(e);
                }
            }
        }

        private List<LogEntry> ProcessFile(string path, Regex pattern)
        {
            List<LogEntry> logEntries = new List<LogEntry>();
            string[] fileText = File.ReadAllLines(path);
            foreach (var line in fileText)
            {
                Match match = pattern.Match(line);
                if (!match.Success)
                {
                    continue;
                }
                LogEntry l = new LogEntry();
                l.SrNumber = serialNumber++;
                l.Date = match.Groups["Date"].Value;
                l.Time = match.Groups["Time"].Value;
                l.Level = match.Groups["Level"].Value;
                l.Message = match.Groups["Message"].Value;
                logEntries.Add(l);
            }
            return logEntries;

        }

        private void WriteLogToCSV(List<LogEntry> logEntries)
        {
            List<string> stringEntries = new List<string>();
            logEntries.ForEach(l => stringEntries.Add(l.ToCSV()));
            Console.WriteLine(logEntries.ElementAt(0));
            File.AppendAllLines(CmdParams.OutputFilePath, stringEntries);
        }
        private string ApplicableRegEx()
        {
            // @"(?mi)^(?<Date>\d{2}/\d{2})\s+(?<Time>\d{2}:\d{2}:\d{2})\s+(?<Level>\w+)\s+(?<Message>(?s:.*?(?=\n\d+:\d+|\r?\z)))";
            //replace \w+ with levels (DEBUG|Error|trace|INFO)
            var logLevel = @"\w+";
            if (CmdParams.Levels != null && CmdParams.Levels.Count > 0)
            {
                logLevel = string.Join("|", CmdParams.Levels);
            }
            var patternString = @"(?mi)^(?<Date>\d{2}/\d{2})\s+" +
            @"(?<Time>\d{2}:\d{2}:\d{2})\s+" +
            @"(?<Level>" + logLevel + @")\s+" +
            @"(?<Message>.*)";
            //@"(?<Message>(:.\w+:))";
            return patternString;

        }

    }
}